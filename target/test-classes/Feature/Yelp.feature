Feature: Search Pizza on Restaurants

  Scenario Outline: Select Pizza in Restaurants
    Given User enters to Yelp website
    When Clicks on Restaurants
    And Enters Pizza in the search engine
    And Selects <Filter>
    And Selects the first option
    Then The information must be displayed

    Examples: 
      | Filter |
      | First  |
      | Second |
      | Third  |
