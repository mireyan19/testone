package StepsDefinitions;

import org.openqa.selenium.firefox.FirefoxDriver;

import Page.YelpClass;

public class Base {
	protected FirefoxDriver driver = OpenBrowser.getDriver();
	protected YelpClass yelp = new YelpClass(driver);
}
